package jwt

import (
	"errors"
	"io/ioutil"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo"
)

//Token types
const (
	SortValidation    = "Validation"
	SortPasswordReset = "Forgot"
	SortSession       = "Session"
)

//Token is a struct representing the jwt token
type Token struct {
	Sort    string
	Created time.Time
	Expiry  time.Time
	Email   string
	jwt.StandardClaims
}

func readPub() (key []byte, err error) {
	data, err := ioutil.ReadFile("./keys/mipjwt.pub")
	if err != nil {
		return data, err
	}
	return data, nil
}

func readSec() (key []byte, err error) {
	data, err := ioutil.ReadFile("./keys/mipjwt")
	if err != nil {
		return data, err
	}
	return data, nil
}

//NewJwt generates a new JWT token
func NewJwt(sort string, email string, hoursValid int) (token string, err error) {

	t := jwt.New(jwt.SigningMethodHS256)

	claims := t.Claims.(jwt.MapClaims)
	claims["Sort"] = sort
	claims["Created"] = time.Now()
	claims["Expiry"] = time.Now().Add(time.Duration(hoursValid) * time.Hour)
	claims["Email"] = email

	sign, err := readSec()
	if err != nil {
		return "", err
	}

	token, signerr := t.SignedString(sign)
	if signerr != nil {
		return "", signerr
	}

	return token, nil
}

//ReadJwt reads a jwt token and determines its validity.
//returns claims if valid and err if not.
func ReadJwt(tokenString string) (token *Token, err error) {
	claims := &Token{}

	sign, err := readSec()
	if err != nil {
		return nil, err
	}

	tkn, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return sign, nil
	})

	if err != nil {
		if err == jwt.ErrSignatureInvalid {
			return nil, err
		}
		return nil, err
	}

	if !tkn.Valid {
		return nil, errors.New("token is invalid")
	}

	if claims.Expiry.Before(time.Now()) {
		return nil, errors.New("token has expired")
	}

	return claims, nil
}

//AuthHeader is used to read the authentication header.
func AuthHeader(c echo.Context) (*Token, error) {
	var token string
	for key, values := range c.Request().Header {
		if key == "Authorization" {
			for _, value := range values {
				token = value
			}
			break
		}
	}

	tkn, err := ReadJwt(token)
	if err != nil {
		return nil, err
	}

	return tkn, nil
}
