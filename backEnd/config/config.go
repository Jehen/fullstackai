package config

import (
	"encoding/json"
	"io/ioutil"
	configmodel "mip/models/config"
)

var conf *configmodel.Config

//LoadConfig loads the config.json into memory and returns a config object
func LoadConfig() (*configmodel.Config, error) {
	f, err := ioutil.ReadFile("./config/config.json")
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(f, &conf); err != nil {
		return nil, err
	}

	return conf, nil
}

//GetConfig returns a config object with all settings.
//Can only be called after LoadConfig has been called at least once.
func GetConfig() *configmodel.Config {
	return conf
}
