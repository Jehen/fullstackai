package system

import (
	"mip/config"
	"mip/db"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
)

//GetSysStatus returns system status
func GetSysStatus(c echo.Context) error {
	sysStat := &models.StatusResponse{
		Status: models.StatusOnline,
	}

	con := db.GetDB()
	if err := con.Ping(con.Context()); err != nil {
		sysStat.DataBase = models.StatusOffline
		sysStat.Details += "DB: " + err.Error() + ", "
		sysStat.Status = models.StatusPartial
	} else {
		sysStat.DataBase = models.StatusOnline
	}

	conf := config.GetConfig()
	resp, err := http.Get(conf.AiServer.BaseAddress + conf.AiServer.Port + conf.AiServer.Routes.SystemRoutes.Status)
	if err != nil || resp.StatusCode != http.StatusOK {
		sysStat.AiServer = models.StatusOffline
		sysStat.Details += "AIserver: " + err.Error()
		sysStat.Status = models.StatusPartial
	} else {
		sysStat.AiServer = models.StatusPartial
	}

	return c.JSON(http.StatusOK, &sysStat)
}
