package aiRequests

import (
	"mip/db"
	"mip/jwt"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
)

func UserPredict(c echo.Context) error {
	token, err := jwt.AuthHeader(c)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Unauthorised", Details: &errStr})
	}

	aii := new(models.AiImage)
	if err := c.Bind(aii); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid image data",
			Details: &errStr,
		}
		return c.JSON(http.StatusBadRequest, &msg)
	}
	uPred := aii.UserPrediction

	con := db.GetDB()
	//var selImg = models.AiImage{}
	if err := con.Model(aii).Where("imgname = ?", &aii.Name).Select(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "given image couldn't be found",
			Details: &errStr,
		}
		return c.JSON(http.StatusNotFound, &msg)
	}

	u := &models.User{Email: &token.Email}
	if err := con.Model(u).Where("useremail = ?", &u.Email).Select(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "user could not be found",
			Details: &errStr,
		}
		return c.JSON(http.StatusNotFound, &msg)
	}

	if u.Role == models.UserRoleAdmin {
		aii.ImgClass = uPred
	}

	if u.Role == models.UserRoleUser {
		if u.ID == aii.UserID {
			aii.UserPrediction = uPred
		} else {
			msg := &models.Response{
				Content: "you may only classify your own images",
				Details: nil,
			}
			return c.JSON(http.StatusUnauthorized, &msg)
		}
	}

	if _, err := con.Model(aii).WherePK().Update(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "could not update image",
			Details: &errStr,
		}
		return c.JSON(http.StatusNotFound, &msg)
	}

	return c.JSON(http.StatusOK, &models.Response{Content: "image succesfully updated"})
}
