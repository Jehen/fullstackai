package aiRequests

import (
	"mip/db"
	"mip/jwt"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
)

func GetAllImgClasses(c echo.Context) error {
	_, err := jwt.AuthHeader(c)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Unauthorised access", Details: &errStr})
	}

	aiSlug := c.Param("aiSlug")
	con := db.GetDB()

	var selClass []models.AiImageClass
	if err := con.Model(&selClass).Where("aitype = ?", aiSlug).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{Content: "Requested AI (" + aiSlug + ") not recognised", Details: &errStr})
	}

	return c.JSON(http.StatusOK, &selClass)
}
