package aiRequests

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"mip/config"
	"mip/db"
	"mip/jwt"
	"mip/models"
	"mip/models/aiServer"
	"net/http"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func ReqRetrain(c echo.Context) error {
	token, err := jwt.AuthHeader(c)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Unauthorised access", Details: &errStr})
	}

	reqRet := new(aiServer.ReqRetrain)
	if err := c.Bind(&reqRet); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{Content: "Invalid request data", Details: &errStr})
	}

	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Where("useremail = ?", token.Email).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{Content: "Given user could not be found", Details: &errStr})
	}

	if selUser.Role != models.UserRoleAdmin {
		det := "You must be an admin to request an AI to be retrained"
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Unauthorised access", Details: &det})
	}

	jReq, err := json.Marshal(&reqRet)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't marshal JSON", Details: &errStr})
	}

	trainReq := aiServer.Retrain{
		SessionId:      uuid.New(),
		SessionStatus:  aiServer.SessionNew,
		AiType:         reqRet.AiSlug,
		UserId:         selUser.ID,
		SessionStarted: time.Now(),
	}
	if _, err := con.Model(&trainReq).Insert(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't create trainig entry in DB", Details: &errStr})
	}

	go DelegateRetrain(jReq, trainReq)

	return c.JSON(http.StatusAccepted, &trainReq)
}

func DelegateRetrain(jReq []byte, eventTicket aiServer.Retrain) {
	conf := config.GetConfig().AiServer
	url := conf.BaseAddress + conf.Port + conf.Routes.AiRoutes.ReqRetrain + "/" + eventTicket.AiType
	resp, err := http.Post(url, "application/json; charset=UTF-8", bytes.NewBuffer(jReq))
	if err != nil || resp.StatusCode != http.StatusOK {
		//TODO record error message
		eventTicket.SessionStatus = aiServer.SessionError
	} else {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			//TODO record error message
			eventTicket.SessionStatus = aiServer.SessionError
		}

		res := []aiServer.SRes{}
		json.Unmarshal(body, &res)

		eventTicket.SessionStatus = aiServer.SessionFinished
		eventTicket.SessionResult = &res
	}

	ut := time.Now()
	eventTicket.SessionUpdated = &ut

	con := db.GetDB()
	if _, err := con.Model(&eventTicket).WherePK().Update(); err != nil {
		print(err.Error())
	}
}
