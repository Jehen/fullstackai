package aiRequests

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"mip/config"
	"mip/db"
	"mip/jwt"
	"mip/models"
	"mip/models/aiServer"
	"net/http"

	"github.com/labstack/echo"
)

func RetrainUpdate(c echo.Context) error {
	token, err := jwt.AuthHeader(c)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Couldn't read auth", Details: &errStr})
	}

	sesId := c.Param("sessionId")
	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Where("useremail = ?", token.Email).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't fetch user from DB", Details: &errStr})
	}

	if selUser.Role != models.UserRoleAdmin {
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "You require admin status to view this content"})
	}

	//Check if the ticket already exists and didn't already end
	var eventTicket aiServer.Retrain
	if err := con.Model(&eventTicket).Where("sessionid = ?", sesId).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{Content: "Couldn't find training session", Details: &errStr})
	}
	if eventTicket.SessionStatus == aiServer.SessionFinished {
		return c.JSON(http.StatusOK, &eventTicket)
	}
	if eventTicket.SessionStatus == aiServer.SessionError {
		return c.JSON(http.StatusOK, &models.Response{Content: "Session ended in errror"})
	}

	conf := config.GetConfig().AiServer
	url := conf.BaseAddress + conf.Port + conf.Routes.AiRoutes.RetrainUpdate + "/" + eventTicket.AiType
	resp, err := http.Get(url)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusFailedDependency, &models.Response{Content: "Couldn't contact AIserver", Details: &errStr})
	}

	//check if the status updated during the request
	if resp.StatusCode == http.StatusNotFound {
		if err := con.Model(&eventTicket).Where("sessionid = ?", sesId).Select(); err != nil {
			errStr := err.Error()
			return c.JSON(http.StatusNotFound, &models.Response{Content: "Couldn't find training session", Details: &errStr})
		}

		if eventTicket.SessionStatus == aiServer.SessionError {
			return c.JSON(http.StatusOK, &models.Response{Content: "Session ended in errror"})
		}

		return c.JSON(http.StatusOK, &eventTicket)
	}

	if resp.StatusCode != http.StatusOK {
		errStr := "Response code: " + fmt.Sprint(resp.StatusCode)
		return c.JSON(http.StatusFailedDependency, &models.Response{Content: "Couldn't contact AI server", Details: &errStr})
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't unmarshal AI response", Details: &errStr})
	}

	res := aiServer.TrainProgress{}
	if err := json.Unmarshal(body, &res); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't unmarshal response", Details: &errStr})
	}

	eventTicket.TrainProgress = &res

	return c.JSON(http.StatusOK, &eventTicket)
}
