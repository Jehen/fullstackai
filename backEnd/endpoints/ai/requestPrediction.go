package aiRequests

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"mip/config"
	"mip/db"
	"mip/jwt"
	"mip/models"
	"mip/models/aiServer"
	configmodel "mip/models/config"
	"net/http"
	"os"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo"
)

func ReqPrediction(c echo.Context) error {
	//verify connection and extract user email
	token, err := jwt.AuthHeader(c)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Unauthorised", Details: &errStr})
	}

	//load the config and check if the requested AI exists
	conf := config.GetConfig()
	var AiHandle *configmodel.AiTypes
	AiSlug := c.Param("slug")
	for _, el := range *conf.AiTypes {
		if AiSlug == el.Slug {
			AiHandle = &el
		}
	}
	if AiHandle == nil {
		return c.JSON(http.StatusNotFound, &models.Response{Content: "Selected AI doesn't exist."})
	}

	//extract image from form and place on location
	img, err := c.FormFile("img")
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{Content: "Couldn't find image", Details: &errStr})
	}

	src, err := img.Open()
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{Content: "Couldn't read image", Details: &errStr})
	}
	defer src.Close()

	//save uploaded image
	fileName := AiHandle.Prefix + "_" + uuid.New().String() + "_" + img.Filename
	filePath := conf.Server.Images.BasePath + AiHandle.ImgPath
	dst, err := os.Create((filePath + "/" + fileName))
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't locate destination", Details: &errStr})
	}

	if _, err := io.Copy(dst, src); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{Content: "Couldn't save image", Details: &errStr})
	}

	//Log image upload into db
	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Column("userid").Where("useremail = ?", token.Email).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{
			Content: "User does not exist",
			Details: &errStr,
		})
	}

	//prepare AI request data
	imgobj := models.AiImage{
		Name:     fileName,
		Location: filePath,
		AIType:   AiHandle.Slug,
		Upload:   time.Now(),
		UserID:   selUser.ID,
	}
	if _, err := con.Model(&imgobj).Insert(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{
			Content: "Could not insert image",
			Details: &errStr,
		})
	}

	reqImg := &aiServer.ImagePrediction{
		Name:     imgobj.Name,
		Location: imgobj.Location,
		AiSlug:   AiHandle.Slug,
	}
	jReqImg, err := json.Marshal(reqImg)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Could not marshal data request",
			Details: &errStr,
		})
	}
	bReqImg := bytes.NewBuffer([]byte(jReqImg))

	//Make AI request
	url := conf.AiServer.BaseAddress + conf.AiServer.Port + conf.AiServer.Routes.AiRoutes.ReqImgPrediction + "/" + AiHandle.Slug
	resp, err := http.Post(url, "application/json; charset=UTF-8", bReqImg)
	if err != nil || resp == nil {
		errStr := err.Error()
		return c.JSON(http.StatusFailedDependency, &models.Response{
			Content: "Could not reach AI server",
			Details: &errStr,
		})
	}
	defer resp.Body.Close()

	//process response
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Could not decode AI response",
			Details: &errStr,
		})
	}

	det := string(body)
	if resp.StatusCode != http.StatusOK {
		return c.JSON(resp.StatusCode, &models.Response{Content: "Error with AI server", Details: &det})
	}

	res := &aiServer.ImageResponse{}
	json.Unmarshal(body, &res)

	//update DB with AI prediction
	imgobj.AIPrediction = &res.PredictionId
	if _, err = con.Model(&imgobj).WherePK().Update(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Could not update database",
			Details: &errStr,
		})
	}

	//fetch name, description and example from DB
	var selImg models.AiImageClass
	if err := con.Model(&selImg).Where("aitype = ?", res.Slug).Where("imgclass = ?", res.PredictionId).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{
			Content: "Discrepancy between AIserver and Database",
			Details: &errStr,
		})
	}

	return c.JSON(http.StatusOK, &models.ImgReqResponse{ImgName: imgobj.Name, Content: selImg})
}
