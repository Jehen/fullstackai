package users

import (
	"mip/db"
	"mip/jwt"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

//Login dispenses a JWT token so the user can access authorised content
func Login(c echo.Context) error {
	u := new(models.User)
	if err := c.Bind(&u); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid user model",
			Details: &errStr,
		}
		return c.JSON(http.StatusBadRequest, &msg)
	}

	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Where("useremail = ?", u.Email).Select(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "User does not exist",
			Details: &errStr,
		}
		return c.JSON(http.StatusNotFound, &msg)
	}

	if err := bcrypt.CompareHashAndPassword([]byte(*selUser.Password), []byte(*u.Password)); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid password and email combination",
			Details: &errStr,
		}
		return c.JSON(http.StatusUnauthorized, &msg)
	}

	token, err := jwt.NewJwt(jwt.SortSession, *u.Email, 48)
	if err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Couldn't generate jwt token",
			Details: &errStr,
		}
		return c.JSON(http.StatusInternalServerError, &msg)
	}

	tkn := &models.Token{
		Body: token,
	}
	return c.JSON(http.StatusOK, &tkn)
}
