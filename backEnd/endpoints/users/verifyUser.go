package users

import (
	"mip/db"
	"mip/jwt"
	"mip/models"
	"net/http"
	"time"

	"github.com/labstack/echo"
)

//VerifyUser verifies the user based on jwt token
func VerifyUser(c echo.Context) error {
	token := c.Param("token")

	tkn, err := jwt.ReadJwt(token)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{
			Content: "Could not read token",
			Details: &errStr,
		})
	}

	if tkn.Sort != jwt.SortValidation {
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Inavlid token sort"})
	}

	con := db.GetDB()
	var verUser models.User
	if err := con.Model(&verUser).Where("useremail = ?", tkn.Email).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{
			Content: "Couldn't find user",
			Details: &errStr,
		})
	}

	if verUser.Verified != nil {
		return c.JSON(http.StatusConflict, &models.Response{Content: "User already verified"})
	}

	var current = time.Now()
	verUser.Verified = &current

	if _, err := con.Model(&verUser).Set("userverified = ?userverified").Where("userid = ?userid").Update(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Couldn't update user",
			Details: &errStr,
		})
	}

	return c.JSON(http.StatusOK, &models.Response{Content: "Success"})
}
