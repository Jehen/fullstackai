package users

import (
	"mip/db"
	"mip/jwt"
	mails "mip/mail"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
)

//PasswordForget allows users to request a password reset
func PasswordForget(c echo.Context) error {
	u := new(models.User)
	if err := c.Bind(&u); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid user model",
			Details: &errStr,
		}
		return c.JSON(http.StatusBadRequest, &msg)
	}

	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Where("useremail = ?", u.Email).Select(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "User does not exist",
			Details: &errStr,
		}
		return c.JSON(http.StatusNotFound, &msg)
	}

	token, err := jwt.NewJwt(jwt.SortPasswordReset, *selUser.Email, 1)
	if err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "A problem occured while generating a token",
			Details: &errStr,
		}
		return c.JSON(http.StatusInternalServerError, &msg)
	}
	url := "http://127.0.0.1:1323/reset/" + token

	if err := mails.SendEmail(&mails.EmailContent{
		To:      []string{*u.Email},
		Subject: "Generic platform password reset",
		Text:    []byte("Dear " + selUser.Name + ",\n\nPlease follow the link below to the password reset page:\n" + url + "\n\nKind regards,\nThe mip team"),
	}); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Couldn't send e-mail",
			Details: &errStr,
		}
		return c.JSON(http.StatusInternalServerError, &msg)
	}

	msg := &models.Response{
		Content: "Success",
	}
	return c.JSON(http.StatusOK, &msg)
}
