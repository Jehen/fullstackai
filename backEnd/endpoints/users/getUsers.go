package users

import (
	"mip/db"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
)

//GetUsers returns all users
func GetUsers(c echo.Context) error {
	con := db.GetDB()
	var users []models.User
	if err := con.Model(&users).Column("userid", "username", "userjoin", "userrole").Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Something went wrong while retrieving the users",
			Details: &errStr,
		})
	}

	return c.JSON(http.StatusOK, &users)
}
