package users

import (
	"mip/config"
	"mip/db"
	"mip/jwt"
	mails "mip/mail"
	"mip/models"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

//AddUser adds a new user
func AddUser(c echo.Context) error {
	//Bind received data to user model
	u := new(models.User)
	if err := c.Bind(&u); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{
			Content: "Invalid user model",
			Details: &errStr,
		})
	}

	if len(*u.Password) < 8 {
		return c.JSON(http.StatusBadRequest, &models.Response{Content: "Invalid password"})
	}

	//Generate password hash from given pasword
	pwd, err := bcrypt.GenerateFromPassword([]byte(*u.Password), bcrypt.DefaultCost)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Couldn't hash password",
			Details: &errStr,
		})
	}
	pwdStr := string(pwd)
	u.Password = &pwdStr

	//Generate token URL and send to email
	token, err := jwt.NewJwt(jwt.SortValidation, *u.Email, 5)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Couldn't generate token",
			Details: &errStr,
		})
	}
	urlConf := config.GetConfig().Server
	url := urlConf.HostAddress + urlConf.Port + strings.ReplaceAll(urlConf.Routes.Users.VerifyUser, ":token", token)

	//Send validation email with JWT token
	if err := mails.SendEmail(&mails.EmailContent{
		To:      []string{*u.Email},
		Subject: "Account validation",
		Text:    []byte("Hi " + u.Name + ", \n\nTo validate your account please visit:\n" + url + "\n\n Kind regards,\nThe mip team"),
	}); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Couldn't send email",
			Details: &errStr,
		})
	}

	plainUUID := new(uuid.UUID)
	u.ID = *plainUUID
	u.JoinDate = time.Now()
	u.Role = models.UserRoleUser

	//Insert new user into DB
	con := db.GetDB()
	if _, err := con.Model(u).Insert(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusBadRequest, &models.Response{
			Content: "Could not insert user",
			Details: &errStr,
		})
	}

	return c.JSON(http.StatusOK, &models.Response{Content: "Success"})
}
