package users

import (
	"mip/db"
	"mip/models"
	"net/http"

	"github.com/google/uuid"
	"github.com/labstack/echo"
)

//GetUser returns a user by ID or email
func GetUser(c echo.Context) error {
	u := new(models.User)
	if err := c.Bind(u); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid UUID or Email",
			Details: &errStr,
		}
		return c.JSON(http.StatusBadRequest, &msg)
	}

	plainUUID := new(uuid.UUID)
	var qs string
	var searchVal interface{}
	if u.ID != *plainUUID {
		qs = "userid = ?"
		searchVal = u.ID
	} else {
		qs = "useremail = ?"
		searchVal = u.Email
	}

	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Column("userid", "username", "userjoin").Where(qs, searchVal).Select(); err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusNotFound, &models.Response{
			Content: "User does not exist",
			Details: &errStr,
		})
	}

	return c.JSON(http.StatusOK, &selUser)
}
