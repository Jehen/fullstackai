package users

import (
	"mip/db"
	"mip/jwt"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
	"golang.org/x/crypto/bcrypt"
)

//PasswordReset resets the user's password
func PasswordReset(c echo.Context) error {
	token := c.Param("token")

	tkn, err := jwt.ReadJwt(token)
	if err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid token",
			Details: &errStr,
		}
		return c.JSON(http.StatusUnauthorized, &msg)
	}

	if tkn.Sort != jwt.SortPasswordReset {
		msg := &models.Response{
			Content: "Invalid token sort",
		}
		return c.JSON(http.StatusUnauthorized, &msg)
	}

	u := &models.User{}
	if err := c.Bind(&u); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Invalid user model",
			Details: &errStr,
		}
		return c.JSON(http.StatusBadRequest, &msg)
	}

	if len(*u.Password) < 8 {
		msg := &models.Response{
			Content: "Invalid password",
		}
		return c.JSON(http.StatusBadRequest, &msg)
	}

	//Generate password hash from given pasword
	pwd, err := bcrypt.GenerateFromPassword([]byte(*u.Password), bcrypt.DefaultCost)
	if err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Couldn't hash password",
			Details: &errStr,
		}
		return c.JSON(http.StatusInternalServerError, msg)
	}
	pwdStr := string(pwd)
	u.Password = &pwdStr

	con := db.GetDB()
	var selUser models.User
	if err := con.Model(&selUser).Where("useremail = ?", tkn.Email).Select(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "User does not exist",
			Details: &errStr,
		}
		return c.JSON(http.StatusNotFound, &msg)
	}

	if _, err := con.Model(&selUser).Set("userpassword = ?", &u.Password).Where("userid= ?", selUser.ID).Update(); err != nil {
		errStr := err.Error()
		msg := &models.Response{
			Content: "Could not update user password",
			Details: &errStr,
		}
		return c.JSON(http.StatusInternalServerError, &msg)
	}

	msg := &models.Response{
		Content: "Success",
	}
	return c.JSON(http.StatusOK, &msg)
}
