package users

import (
	"mip/jwt"
	"mip/models"
	"net/http"

	"github.com/labstack/echo"
)

//Refresh allows tokens to be refreshed if the given token is valid
func Refresh(c echo.Context) error {
	var token string
	for key, values := range c.Request().Header {
		if key == "Authorization" {
			for _, value := range values {
				token = value
			}
			break
		}
	}

	tkn, err := jwt.ReadJwt(token)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusUnauthorized, &models.Response{
			Content: "No valid token", Details: &errStr})
	}

	if tkn.Sort != jwt.SortSession {
		return c.JSON(http.StatusUnauthorized, &models.Response{Content: "Invalid token sort"})
	}

	newtkn, err := jwt.NewJwt(tkn.Sort, tkn.Email, 48)
	if err != nil {
		errStr := err.Error()
		return c.JSON(http.StatusInternalServerError, &models.Response{
			Content: "Couldn't generate new token", Details: &errStr})
	}

	newToken := &models.Token{
		Body: newtkn,
	}
	return c.JSON(http.StatusOK, &newToken)
}
