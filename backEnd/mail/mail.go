package mails

import (
	"mip/config"
	"net/smtp"
	"net/textproto"

	"github.com/jordan-wright/email"
)

//Credentials are used to set your email credentials
type Credentials struct {
	Email      string
	Password   string
	Address    string
	HostString string
}

//EmailContent is used to send an email with all the contents
type EmailContent struct {
	To      []string
	Subject string
	Text    []byte
}

var cred Credentials

//InitCredentials sets the initial credential values.
func InitCredentials() {
	conf := config.GetConfig()

	SetCredentials(&Credentials{
		Email:      conf.Mail.EmailAddress,
		Password:   conf.Mail.Password,
		Address:    conf.Mail.Address,
		HostString: conf.Mail.HostString,
	})
}

//SetCredentials is used to set your credentials before sending an e-mail
//If left unchanged the last set of credentials will be used
func SetCredentials(credentials *Credentials) {
	cred.Email = credentials.Email
	cred.Password = credentials.Password
	cred.Address = credentials.Address
	cred.HostString = credentials.HostString
}

//SendEmail sends an email using the EmailContent struct
func SendEmail(contents *EmailContent) error {
	e := &email.Email{
		To:      contents.To,
		From:    cred.Email,
		Subject: contents.Subject,
		Text:    contents.Text,
		Headers: textproto.MIMEHeader{},
	}

	//NOTE might need reworking later to make safer by not just using plainAuth.
	err := e.Send(cred.Address, smtp.PlainAuth("", cred.Email, cred.Password, cred.HostString))
	if err != nil {
		return err
	}
	return nil
}
