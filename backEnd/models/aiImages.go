package models

import (
	"time"

	"github.com/google/uuid"
)

type AiImage struct {
	Name           string    `pg:"imgname,pk" json:"name"`
	Location       string    `pg:"imglocation" json:"location"`
	Upload         time.Time `pg:"imgupload" json:"uploadDate"`
	UserID         uuid.UUID `pg:"userid" json:"userID"`
	AIType         string    `pg:"aitype" json:"aiType"`
	AIPrediction   *string   `pg:"aiprediction" json:"aiPrediction"`
	ImgClass       *string   `pg:"imgclass" json:"class"`
	UserPrediction *string   `pg:"userprediction" json:"userPrediction"`
	tableName      struct{}  `pg:"aiimgs"`
}

type AiImageClass struct {
	AiType      string   `pg:"aitype" json:"aiType"`
	Class       string   `pg:"imgclass" json:"class"`
	Name        string   `pg:"imgname" json:"name"`
	Description *string  `pg:"imgdesc" json:"desc"`
	Example     *string  `pg:"imgexample" json:"example"`
	tableName   struct{} `pg:"aiimgclasses"`
}
