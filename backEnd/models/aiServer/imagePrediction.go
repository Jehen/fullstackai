package aiServer

type ImagePrediction struct {
	Name     string `json:"name"`
	Location string `json:"location"`
	AiSlug   string `json:"aiSlug"`
}
