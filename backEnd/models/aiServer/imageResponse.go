package aiServer

type ImageResponse struct {
	PredictionId string  `json:"predictionId"`
	Slug         string  `json:"slug"`
	Prediction   string  `json:"prediction"`
	Description  *string `json:"description"`
	Example      *string `json:"example"`
}
