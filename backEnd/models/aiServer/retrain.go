package aiServer

import (
	"time"

	"github.com/google/uuid"
)

const (
	SessionNew      = "NEW"
	SessionError    = "ERROR"
	SessionFinished = "FINISHED"
)

type ReqRetrain struct {
	AiSlug string
	Epochs int
}

type Retrain struct {
	SessionId      uuid.UUID      `pg:"sessionid,pk"`
	SessionStatus  string         `pg:"sessionstatus"`
	AiType         string         `pg:"aitype"`
	UserId         uuid.UUID      `pg:"userid"`
	SessionStarted time.Time      `pg:"sessionstarted"`
	SessionUpdated *time.Time     `pg:"sessionupdated"`
	SessionResult  *[]SRes        `pg:"sessionresult"`
	TrainProgress  *TrainProgress `pg:"-"`
	tableName      struct{}       `pg:"aitraining"`
}

type SRes struct {
	Epoch         int
	TrainLoss     float32
	TestLoss      float32
	TrainAccuracy float32
	TestAccuracy  float32
}

type TrainProgress struct {
	Epoch   int
	Stage   string
	Ittr    int
	MaxIttr int
}
