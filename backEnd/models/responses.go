package models

const (
	StatusOnline  = "ONLINE"
	StatusDown    = "DOWN"
	StatusOffline = "OFFLINE"
	StatusPartial = "PARTIAL"
)

//Response is the default JSON response
type Response struct {
	Content string  `json:"content"`
	Details *string `json:"details"`
}

//Token is the default JWT token return
type Token struct {
	Body string `json:"body"`
}

type StatusResponse struct {
	Status   string
	AiServer string
	DataBase string
	Details  string
}

type ImgReqResponse struct {
	ImgName string
	Content AiImageClass
}
