package models

import (
	"time"

	"github.com/google/uuid"
)

//Various user model constants
const (
	UserRoleUser  = "USER"
	UserRoleAdmin = "ADMIN"
)

//User is the base user model
type User struct {
	ID        uuid.UUID  `pg:"userid,pk" json:"id"`
	Name      string     `pg:"username" json:"name"`
	Email     *string    `pg:"useremail" json:"email"`
	Password  *string    `pg:"userpassword" json:"password"`
	JoinDate  time.Time  `pg:"userjoin" json:"joinDate"`
	Verified  *time.Time `pg:"userverified" json:"verified"`
	Role      string     `pg:"userrole" json:"role"`
	tableName struct{}   `pg:"users"`
}
