package configmodel

//Email contains all the email configiration settings
type Email struct {
	EmailAddress string `json:"email"`
	Password     string
	Address      string
	HostString   string
}
