package configmodel

//Config holds all the configuration data
type Config struct {
	Server   ServerConfig
	DataBase DbCreds
	Mail     Email
	AiServer AiServer
	AiTypes  *[]AiTypes
}

//ServerConfig is a struct that holds the config data for the server settings
type ServerConfig struct {
	HostAddress string
	Port        string
	Routes      RouteStr
	Images      Images
}
