package configmodel

type AiTypes struct {
	ImgPath string
	Prefix  string
	Slug    string
}

type AiServer struct {
	BaseAddress string
	Port        string
	Routes      Routes
}

type Routes struct {
	SystemRoutes SystemRoutes `json:"system"`
	AiRoutes     AiRoutes     `json:"aiRoutes"`
}

type SystemRoutes struct {
	Status string
}

type AiRoutes struct {
	ReqImgPrediction string `json:"requestImgPrediction"`
	ReqRetrain       string `json:"requestRetrain"`
	RetrainUpdate    string
}
