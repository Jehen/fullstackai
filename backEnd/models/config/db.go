package configmodel

//DbCreds contains all the database configuration data
type DbCreds struct {
	User     string
	Password string
	Database string
}
