package configmodel

//RouteStr contains all routes
type RouteStr struct {
	System System
	Users  Users
	AIs    AIs
}

//System holds all the system addresses
type System struct {
	Status string
}

type Users struct {
	AddUser        string
	GetUser        string
	GetUsers       string
	Login          string
	PasswordForget string
	PasswordReset  string
	TokenRefresh   string `json:"refresh"`
	VerifyUser     string
}

type AIs struct {
	ReqImgPrediction  string `json:"requestImgPrediction"`
	UserImgPrediction string
	GetAllImgClasses  string
	RequestRetrain    string
	RetrainUpdate     string
}
