package db

import (
	"mip/config"

	"github.com/go-pg/pg/v10"
)

//DB is the DB connection pool
var DB *pg.DB

//Connect Connects to the db
func Connect() {
	conf := config.GetConfig()

	db := pg.Connect(&pg.Options{
		User:     conf.DataBase.User,
		Password: conf.DataBase.Password,
		Database: conf.DataBase.Database,
	})

	DB = db
}

//GetDB gets a pointer to the Db connection pool.
func GetDB() *pg.DB {
	return *&DB
}
