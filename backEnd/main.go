package main

import (
	"fmt"
	"mip/config"
	"mip/db"
	mails "mip/mail"
	"mip/routes"

	"github.com/labstack/echo"
)

func main() {
	fmt.Println("Booting...")

	conf, err := config.LoadConfig()
	if err != nil {
		panic(err)
	}

	fmt.Printf("%+v", conf)

	//connect to DB
	db.Connect()

	//init email credentials
	mails.InitCredentials()

	//init echo server
	e := echo.New()

	//set up routes
	routes.SetSysRoutes(e)
	routes.SetUserRoutes(e)
	routes.SetAIRoutes(e)

	//start server
	e.Logger.Fatal(e.Start(conf.Server.Port))
}
