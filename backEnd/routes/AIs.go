package routes

import (
	"mip/config"
	ai "mip/endpoints/ai"

	"github.com/labstack/echo"
)

//SetUserRoutes is a test func
func SetAIRoutes(e *echo.Echo) {
	aiRoutes := config.GetConfig().Server.Routes.AIs

	e.POST(aiRoutes.ReqImgPrediction, ai.ReqPrediction)
	e.PATCH(aiRoutes.UserImgPrediction, ai.UserPredict)
	e.GET(aiRoutes.GetAllImgClasses, ai.GetAllImgClasses)
	e.PUT(aiRoutes.RequestRetrain, ai.ReqRetrain)
	e.GET(aiRoutes.RetrainUpdate, ai.RetrainUpdate)
}
