package routes

import (
	"mip/config"
	"mip/endpoints/users"

	"github.com/labstack/echo"
)

//SetUserRoutes is a test func
func SetUserRoutes(e *echo.Echo) {
	conf := config.GetConfig()
	uRoutes := conf.Server.Routes.Users

	e.GET(uRoutes.GetUsers, users.GetUsers)
	e.GET(uRoutes.GetUser, users.GetUser)
	e.POST(uRoutes.AddUser, users.AddUser)
	e.PATCH(uRoutes.VerifyUser, users.VerifyUser)
	e.GET(uRoutes.PasswordForget, users.PasswordForget)
	e.PATCH(uRoutes.PasswordReset, users.PasswordReset)
	e.GET(uRoutes.Login, users.Login)
	e.GET(uRoutes.TokenRefresh, users.Refresh)
}
