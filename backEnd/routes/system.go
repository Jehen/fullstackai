package routes

import (
	"mip/config"
	"mip/endpoints/system"

	"github.com/labstack/echo"
)

//SetSysRoutes is a test func
func SetSysRoutes(e *echo.Echo) {
	conf := config.GetConfig()
	e.GET(conf.Server.Routes.System.Status, system.GetSysStatus)
}
