import numpy as np
import torch
import matplotlib.pyplot as plt
from ai.trafficSigns import trafficSignNet as tsn
from tqdm import tqdm
import torch.optim as opt
import torch.nn as nn
import time

class tsTestTrain():
    def __init__(self, device, IMG_SIZE, TRN_DATA, TST_DATA, NTW_NAME, LOG_DATA):
        self.device = device
        self.IMG_SIZE = IMG_SIZE
        self.TRN_DATA = TRN_DATA
        self.TST_DATA = TST_DATA
        self.NTW_NAME = NTW_NAME
        self.LOG_DATA = LOG_DATA

        self.net = tsn.signNet(self.IMG_SIZE).to(device=self.device)

        self.progress = {"epoch": 0, "stage": "inactive", "ittr": 0, "maxIttr": 0}


    def trainNet(self, batchSize = 100, epochs = 1, showExample = False, completeLog = True):
        SAMPLE, LABEL, test_SAMPLE, test_LABEL = self.prepData()    

        if showExample == True:
            print(LABEL[0])
            plt.imshow(SAMPLE[0], cmap="gray")
            plt.show()

        #net = tsn.signNet(self.IMG_SIZE).to(device=self.device)
        #print(net)

        optimiser = opt.Adam(self.net.parameters(), lr=0.001)
        loss_func = nn.MSELoss()

        with open(self.LOG_DATA, "w+") as sf:
            sf.write("epoch,trainLoss,testLoss,trainAccuracy,testAccuracy\n")
            for epoch in range(epochs):
                self.progress["epoch"] = epoch
                self.progress["stage"] = "training"
                self.progress["maxIttr"] = len(LABEL)
                #train net
                for i in tqdm(range(0, len(LABEL), batchSize), desc=f"epoch: {epoch}"):
                    
                    self.progress["ittr"] = i

                    sample_batch = SAMPLE[i:i+batchSize].view(-1, 1, self.IMG_SIZE, self.IMG_SIZE)
                    label_batch = LABEL[i:i+batchSize]

                    self.net.zero_grad()
                    outputs = self.net(sample_batch)
                    loss = loss_func(outputs, label_batch)
                    loss.backward()
                    optimiser.step()
                
                if completeLog:
                    self.progress["stage"] = "checking loss"
                    self.progress["maxIttr"] = len(test_SAMPLE)

                    #check loss against validation data, but don't train against validation data
                    for i in tqdm(range(0, len(test_SAMPLE), batchSize), desc="Loss testing"):

                        self.progress["ittr"] = i

                        testSamples = test_SAMPLE[i:i+batchSize].view(-1, 1, self.IMG_SIZE, self.IMG_SIZE)
                        testLabels = test_LABEL[i:i+batchSize]

                        self.net.zero_grad()
                        testOut = self.net(testSamples)
                        testLoss = loss_func(testOut, testLabels)

                    acc = self.testNet(test_SAMPLE, test_LABEL, SAMPLE, LABEL)
                    sf.write(f"{epoch},{loss},{testLoss},{acc[0]},{acc[1]}\n")
                else:
                    sf.write(f"{epoch},{loss},0,{acc[0]},0\n")
                
                print(f"Epoch: {epoch}. Loss: {loss}. Accuracy: {acc[0]}")
                self.progress["ittr"] = 0
                self.progress["maxIttr"] = 0

        self.progress["stage"] = "inactive"
        self.progress["epoch"] = 0
        torch.save(self.net.state_dict(), self.NTW_NAME)


    def testNet(self, tSample, tLabel, sample, label) -> list:
        return [self.getNetAccuracy(sample, label), self.getNetAccuracy(tSample, tLabel)]


    def getNetAccuracy(self, sample, label) -> float:
        total = 0
        correct = 0

        self.progress["stage"] = "checking accuracy"
        self.progress["maxIttr"] = len(sample)

        with torch.no_grad():
            for i in tqdm(range(len(sample)), desc="Accuracy testing"):
                self.progress["ittr"] = i

                expected = torch.argmax(label[i])
                output = self.net(sample[i].view(-1, 1, self.IMG_SIZE, self.IMG_SIZE))
                prediction = torch.argmax(output)
            
                if prediction == expected:
                    correct += 1
                total += 1
        
        print(f"correct: {correct} out of: {total}")
        return (correct/total)


    def prepData(self) -> tuple:
        trainingData = np.load(self.TRN_DATA, allow_pickle=True)
        testingData = np.load(self.TST_DATA, allow_pickle=True)
        print("loaded train and test data")
        print(f"train data len: {len(trainingData)}")
        print(f"test data len: {len(testingData)}")

        SAMPLE, LABEL = self.formatData(trainingData)
        tSAMPLE, tLABEL = self.formatData(testingData)
        print(f"test data samples: {len(tSAMPLE)}, labels: {len(tLABEL)}")

        return  (SAMPLE, LABEL, tSAMPLE, tLABEL)


    #normalise all input to be between 0 and 1 and to split the sample and label
    def formatData(self, dataset) -> tuple:
        SAMPLE = torch.Tensor([e[0] for e in dataset]).view(-1, self.IMG_SIZE, self.IMG_SIZE)
        SAMPLE = SAMPLE/255.0
        LABEL = torch.Tensor([e[1] for e in dataset])

        SAMPLE, LABEL = SAMPLE.to(self.device), LABEL.to(self.device)

        return (SAMPLE, LABEL)
    
    def getProgress(self) -> dict:
        return self.progress