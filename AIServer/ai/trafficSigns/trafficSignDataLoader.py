import os
import cv2
import numpy as np
from tqdm import tqdm
import csv
import pandas as ps
import shutil


class TrafficSignLoader():
    def __init__(self, IMG_SIZE, DATASET_PATH, VAL_DATA_PATH, VAL_LABELS_PATH, DATA_DESTINY, VAL_DESTINY):
        self.IMG_SIZE = IMG_SIZE
        self.PATH = DATASET_PATH
        self.VAL_DATA_PATH = VAL_DATA_PATH
        self.DATA_DESTINY = DATA_DESTINY
        self.VAL_DESTINY = VAL_DESTINY

        self.LABELS = ps.read_csv(VAL_LABELS_PATH)

        self.processed_data = []


    #prepTrainingData will prepare training data and process it into a numpy array.
    #it will also balance the dataset by duplocating or removing files until it reaches the balancingNr.
    #it will do so with stepSize, which is set to 10 default (e.g it will delete or duplicate every nth file)
    def prepTrainingData(self, balancingNr, stepSize = 10):
        err = 0
        for d in tqdm(os.listdir(self.PATH), desc="Checking data"):
            presentFiles = 0

            #count files in dir
            for _ in tqdm(os.listdir(f"{self.PATH}/{d}"), desc="Counting"):
                presentFiles += 1
            
            #delete files if they number above the desired number
            if presentFiles > balancingNr:
                for _ in tqdm(range(presentFiles-balancingNr), desc="Deleting files"):
                    step = 0

                    for df in os.listdir(f"{self.PATH}/{d}"):
                        step += 1

                        if step == stepSize:
                            os.remove(f"./{self.PATH}/{d}/{df}")
                            presentFiles -= 1
                            step = 0
                        
                        if presentFiles == balancingNr:
                            break
                    if presentFiles == balancingNr:
                        break
            
            #duplicate files if they are below the desired number
            if presentFiles < balancingNr:
                for _ in tqdm(range(balancingNr-presentFiles), desc="Duplicating files"):
                    step = 0

                    for cf in os.listdir(f"{self.PATH}/{d}"):
                        step += 1

                        if step == stepSize:
                            presentFiles += 1
                            shutil.copy(f"./{self.PATH}/{d}/{cf}", f"./{self.PATH}/{d}/{presentFiles}.jpg")
                            step = 0

                        if presentFiles == balancingNr:
                            break
                    if presentFiles == balancingNr:
                        break

            #process files into numpy array
            for f in tqdm(os.listdir(f"{self.PATH}/{d}"), desc="Creating dataset"):
                if "jpg" in f:
                    try:
                        img = cv2.imread(f"{self.PATH}/{d}/{f}", cv2.IMREAD_GRAYSCALE)
                        img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
                        self.processed_data.append([np.array(img), np.eye(43)[int(d)]])

                    except Exception as e:
                        print(f"{self.PATH}/{d}/{f}", str(e))
                        err += 1
        
        print(f"Errors processing training data: {err}")
        
        np.random.shuffle(self.processed_data)
        np.save(self.DATA_DESTINY, self.processed_data)
        self.processed_data = []


    #prepare and save the test/validation data
    def prepTestData(self):
        err = 0
        print(self.LABELS.iloc[0])
        for i in tqdm(range(len(self.LABELS)), desc="Processing validation images"):
            try:
                img = cv2.imread(f"{self.VAL_DATA_PATH}/{self.LABELS.iloc[i, 0]}", cv2.IMREAD_GRAYSCALE)
                img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
                self.processed_data.append([np.array(img), np.eye(43)[int(self.LABELS.iloc[i,1])]])

            except Exception as e:
                print(f"{self.VAL_DATA_PATH}/{self.LABELS.iloc[i, 0]}", str(e))
                err += 1

        print(f"Errors processing validation data: {err}")

        np.random.shuffle(self.processed_data)
        np.save(self.VAL_DESTINY, self.processed_data)
        self.processed_data = []
