import numpy as np
from ai.trafficSigns import trafficSignDataLoader as tsdl
import torch
import matplotlib.pyplot as plt
from ai.trafficSigns import trafficSignTraining as tst
from tqdm import tqdm
import pandas as ps
from ai.trafficSigns import trafficSignNet as tsn
import os
from pprint import pp
import cv2

class TrafficSignAI():
    def __init__(self, basePath=".", backendUser=False):
        self.device = torch.device("cpu")
        if torch.cuda.is_available():
            self.device = torch.device("cuda:0")

        self.BackEndOnly = backendUser
        self.path = basePath
        
        #network and training vars
        self.EPOCH = 10
        self.IMG_SIZE = 50

        #network and data savefiles
        self.NTW_NAME = f"{basePath}/networks/tsNet"
        self.TRN_DATA = f"{basePath}/prepData/tsTrain.npy"
        self.TST_DATA = f"{basePath}/prepData/tsTest.npy"
        self.LOG_DATA = f"{basePath}/trainLogs/tsEpochs{self.EPOCH}.csv"
        self.DATASET_PATH = f"{basePath}/datasets/trafficSigns/dataset"
        self.VAL_DATA_PATH = f"{basePath}/datasets/trafficSigns/validationData"
        self.VAL_LABELS = f"{basePath}/datasets/trafficSigns/test_labels.csv"

        self.trainer = None


    def setEpoch(self, epoch):
        self.EPOCH = epoch
        self.LOG_DATA = f"{self.path}/trainLogs/tsEpochs{epoch}.csv"


    def buildData(self):
        dataLoader = tsdl.TrafficSignLoader(self.IMG_SIZE, self.DATASET_PATH, self.VAL_DATA_PATH, self.VAL_LABELS, self.TRN_DATA, self.TST_DATA)
        dataLoader.prepTrainingData(600)
        dataLoader.prepTestData()
    
    
    def train(self) -> dict:
        self.trainer = tst.tsTestTrain(self.device, self.IMG_SIZE, self.TRN_DATA, self.TST_DATA, self.NTW_NAME, self.LOG_DATA)
        self.trainer.trainNet(epochs=self.EPOCH)

        #load and display training and testing losses and accuracies by epoch.
        if self.BackEndOnly:
            res = ps.read_csv(self.LOG_DATA)

            res.plot(x="epoch", y=["trainLoss", "testLoss", "trainAccuracy", "testAccuracy"], color=["red", "orange", "blue", "green"], xticks=range(self.EPOCH))
            plt.title("model fitness and loss by epoch")
            plt.show()
        
        return ps.read_csv(self.LOG_DATA).to_dict('records')

    def getTrainProgress(self) -> dict:
        return self.trainer.getProgress()

    def predict(self, path):
        try:
            img = None

            absolatedPath = os.path.abspath(path)
            img = cv2.imread(absolatedPath, cv2.IMREAD_GRAYSCALE)
            img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))
            
            sample = torch.Tensor(img).view(-1, self.IMG_SIZE, self.IMG_SIZE)
            sample = sample/255.0

            sample = sample.to(device=self.device)

            net = tsn.signNet(self.IMG_SIZE).to(device=self.device)
            net.load_state_dict(torch.load(self.NTW_NAME))
            net.eval()

            with torch.no_grad():
                output = net(sample.view(-1, 1, self.IMG_SIZE, self.IMG_SIZE))
                prediction = torch.argmax(output)

            res = prediction.to("cpu").numpy()

            return str(res)

        except Exception as e:

            return str(e)        
