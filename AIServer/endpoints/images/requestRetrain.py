import json
from flask import Flask, request, jsonify
from ai.trafficSigns import trafficSignMain as tsm
import os

def initRetrain(app, config, relatDir):
    retrainAddr = config["server"]["routes"]["imagesAI"]["reqRetrain"]
    sessionAddr = config["server"]["routes"]["imagesAI"]["trainUpdate"]

    aiHandle = {}

    @app.route(retrainAddr, methods=['POST'])
    def retrain(slug):
        payload = json.loads(request.data)
        result = None

        if slug in aiHandle:
            return jsonify({"content": "AI currently busy"}), 429

        #if the given slug matches a known slug pass data to given AI.
        if slug == config["server"]["aiSlugs"]["trafficSigns"]:
            aiHandle[slug] = tsm.TrafficSignAI(basePath=relatDir+"/trafficSigns")
        else:
            return jsonify({"content": "AI doesn't exist"}), 404

        aiHandle[slug].setEpoch(payload["Epochs"])
        result = aiHandle[slug].train()
        aiHandle.pop(slug, None)

        return jsonify(result), 200
    

    @app.route(sessionAddr, methods=['GET'])
    def getSessionUpdate(slug):
        if slug not in aiHandle:
            return jsonify({"content": "Session already finished or not found"}), 404
        
        return jsonify(aiHandle[slug].getTrainProgress()), 200
