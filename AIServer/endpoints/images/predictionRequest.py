import json
from flask import Flask, request, jsonify
from ai.trafficSigns import trafficSignMain as tsm
import os

def initPredictionReq(app, config, relatDir):
    address = config["server"]["routes"]["imagesAI"]["imgReqPred"]

    @app.route(address, methods=['POST'])
    def predict(slug):
        payload = json.loads(request.data)

        #check if given location exists
        path = payload["location"] + "/" + payload["name"]
        if os.path.exists(path) != True:
            return jsonify({"Content": "Path does not exist"}), 400

        #if the given slug matches a known slug pass data to given AI.
        if slug == config["server"]["aiSlugs"]["trafficSigns"]:
            tsAi = tsm.TrafficSignAI(basePath=relatDir+"/trafficSigns", backendUser=False)
            prediction = tsAi.predict(path)
        else:
            return jsonify({"Content": "AI doesn't exist"})

        return jsonify({"predictionId": prediction, "slug": slug}), 200