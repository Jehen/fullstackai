import json
from flask import Flask, request, jsonify


def initStatus(app, config):
    address = config["server"]["routes"]["system"]["status"]

    @app.route(address)
    def status():
        return jsonify({"status": "online"})