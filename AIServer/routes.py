from endpoints.system import status
from endpoints.images import predictionRequest as ipr
from endpoints.images import requestRetrain as rrt
import os
import json

def innitRoutes(app):
    #load config
    config = None
    with open('config.json') as f:
        config = json.load(f)

    #set up relative directory path for AIs.
    relativePath = os.getcwd()+"/ai"

    #launch routes
    status.initStatus(app, config)
    
    ipr.initPredictionReq(app, config, relatDir=relativePath)
    rrt.initRetrain(app, config, relatDir=relativePath)