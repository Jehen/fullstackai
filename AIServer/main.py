import json
from flask import Flask
import endpoints as eps
import routes

if __name__ == "__main__":
    app = Flask("AiServer")

    routes.innitRoutes(app)

    app.run()