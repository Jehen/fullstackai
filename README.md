# FULL STACK AI
Is a project for me to play around with, but also serves as portfolio.
Do with this project what you like.

## Tech stack
- Go lang
- Echo
- Python (3)
- Flask
- PyTorch
- React
- PostgreSQL
- Go-pg

## Goals
### Concrete
- AiServer (Python) which can host AIs and process requests & data.
- Back-end (Go lang) which handles all the logins and data processing and talks to the AiServer and DB.
- Front-end (React) which allows users and admins to interact with AIs (by e.g. requesting predictions and training the AI).

### Personal/abstract
- To serve as an easy reference point/example/proof of concept for a restful ai based application
- (for me) To learn in an environment where my bugs won't cause any harm or inconveniences
- (for me) To serve as part of a portfolio to display some of the skills I have
This also means that not all things will be (fully) implemented initially.

## Acknowledgements
In this section I address (weird) choices I made and/or therefore also possible future changes,
as well as things I am still working on improving. And credits/links to resources used.

### Intended set-up
This project is developed with the idea that it should be ran over an https connection.
And that the AiServer is only internally available in a closed network.
That way the only part that has to handle complex security and logins is the Go back-end.
This is also done so that the AiServer can function as a service to the Go back-end (kind of like a microservice).
Incase the AiServer is down or being maintained the go server is still approachable and can update the client (same with the DB).

### Intended/Imaginary use
A simple platform wherein AI/Data professionals can open up their AI to users in a modular fashion.
As well as allowing the professionals to edit their datasets, train their AIs and more from a distance and/or through a simple interface.
And for the users to be able to upload their data they want evaluated by the AI.
The users are also able to upload their own 'prediction'/'classification' of what their uploaded data is or should be.
Potentially giving insight to professionals about the gap between AI performance and human expectations.

### AI
#### Traffic signs
The traffic signs AI uses a dataset from [this kaggle dataset](https://www.kaggle.com/flo2607/traffic-signs-classification).
The images included are (I am pretty certain) all european type traffic signs, this however was not stated anywhere.
I reformatted the dataset to some degree and added descriptions myself.

##### Data balancing
The dataset was very unbalanced with some sample sizes being as little as 150, and others as large as 1500.
So I decided to cull the larger samples and duplicate the smaller samples until all classes numbered 600.
This number can be changed in the TrafficSignDataLoader.py file.
The method by which the files were duplicated and culled was a simple interval step.
E.g. duplicate/cull every n-th file and repeat until the desired number reached.

##### Endpoints
For the time being I do not plan on making end-points for dataset management.
Not until I have more other parts implemented.

### Unit tests
I am not using unit tests with this project (yet at least).
This is mostly because I just wanted to develop quickly and learn new things.

### Repeating code
Currently there are still quite a few places in which the same code is ran.
Ideally this would all be turned into one function.
This will be cleaned up every time I reach a new baseline. (e.g. patches after a major version)

### Venv
For the AIserver I use a python venv (_AiServer), Which can be created by
` python -m venv _AiServer `
And to install all the required dependencies
` pip install -r requirements.txt `

### Email & DB
The email address used for verification is a throwaway gmail account.
Still I would appreciate it if you wouldn't 'hack' it.
Normally you would obviously use some kind of inhouse e-mail account, with more secure login methods.

The DB also has all the login credentials set to "mip".
But the idea, is just like with the AiServer to make it only available on the local-host/network.

## Other
This file will be updated as the project gets updated, check the development branche when available.
